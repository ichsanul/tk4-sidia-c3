from django.shortcuts import render, redirect, HttpResponseRedirect
from django.db import connection, IntegrityError
from django.db.utils import InternalError
from django.contrib import messages
from datetime import datetime
from datetime import date
import random


def index(request):
    context = {
        "role_this_user": request.session["role_this_user"]
    }
    if request.session['member_id'] != None:
        context['is_authenticated'] = True
    # Buat otoritas tiap role
    if request.session['role_this_user'] == 'ADMIN_SATGAS':
        context['is_admin_satgas'] = True
    elif request.session['role_this_user'] == 'SUPPLIER':
        context['is_supplier'] = True
    elif request.session['role_this_user'] == 'PETUGAS_DISTRIBUSI':
        context['is_petugas_distribusi'] = True
    elif request.session['role_this_user'] == 'PETUGAS_FASKES':
        context['is_petugas_faskes'] = True
    context['username'] = request.session['member_id']
    return render(request, 't2/index.html', context)


#Trigger 3
#CRUD Permohonan Sumber Daya
#Read
def permohonan_read(request):
    response = { }
    if request.session['role_this_user'] != 'ADMIN_SATGAS' and request.session['role_this_user'] != 'PETUGAS_FASKES':
        return redirect("/sumberdaya")
    response['permohonans'] = []
    if request.session['member_id'] != None:
        response['is_authenticated'] = True
    else:
        return redirect("/")
    with connection.cursor() as cursor:
        cursor.execute(f"""
                    SELECT p.no_transaksi_sumber_daya, p.username_petugas_faskes, p.catatan, 
                    t.tanggal, t.total_item, t.total_berat, r.kode_status_permohonan
                    FROM PERMOHONAN_SUMBER_DAYA_FASKES AS p LEFT OUTER JOIN TRANSAKSI_SUMBER_DAYA AS t
                    ON p.no_transaksi_sumber_daya = t.nomor
                    LEFT OUTER JOIN RIWAYAT_STATUS_PERMOHONAN AS r ON p.no_transaksi_sumber_daya = r.nomor_permohonan""")
        fetched = cursor.fetchall()
        for i in range(len(fetched)):
            response['permohonans'].append([
                fetched[i][0], fetched[i][1], fetched[i][2],
                fetched[i][3], fetched[i][4], fetched[i][5], fetched[i][6]])
    return render(request, 't2/permohonan-read.html', response)

def permohonan_create(request):
    response = {}
    if request.session['member_id'] != None:
        response['is_authenticated'] = True
    if request.session['role_this_user'] != 'PETUGAS_FASKES':
        return redirect("/sumberdaya/permohonan")
    if request.method == "POST":
        with connection.cursor() as cursor:
            try:
                date_today = date.today()
                cursor.execute("SELECT nomor FROM TRANSAKSI_SUMBER_DAYA")
                new_no_trans = len(cursor.fetchall()) + 1
                useridnow = request.session['member_id']

                cursor.execute(f"""
                        INSERT INTO TRANSAKSI_SUMBER_DAYA (nomor, tanggal, total_berat, total_item) VALUES
                        ('{new_no_trans}', '{date_today}', 0, {request.POST["jumlah"]}) """)


                cursor.execute(f"""
                                        INSERT INTO PERMOHONAN_SUMBER_DAYA_FASKES (no_transaksi_sumber_daya, username_petugas_faskes, 
                                        catatan) VALUES ('{new_no_trans}', '{useridnow}', '{request.POST['catatan']}') """)

                cursor.execute(f"""
                                                INSERT INTO DAFTAR_ITEM 
                                                VALUES ('{new_no_trans}', 1, {request.POST["jumlah"]}, {request.POST["kode_item"]},
                                                  0, 0) """)

                cursor.execute(f"""
                                                INSERT INTO RIWAYAT_STATUS_PERMOHONAN (kode_status_permohonan, nomor_permohonan, username_admin, tanggal)
                                                VALUES ("REQ", '{new_no_trans}', "jogilvy0", 
                                                '{date_today}') """)

                messages.add_message(request, messages.SUCCESS, f"Penambahan berhasil!")
                return redirect("/sumberdaya/permohonan")

            except:
                messages.add_message(request, messages.WARNING, f"Terdapat masalah.")

    else:
        response['code'] = []
        with connection.cursor() as cursor:
            #Untuk no transaksi
            cursor.execute("SELECT nomor FROM TRANSAKSI_SUMBER_DAYA")
            fetched = len(cursor.fetchall()) + 1
            response['notrans'] = fetched
            response['notranscoded'] = "TREQ" + "{:03d}".format(fetched)
            cursor.execute("SELECT * FROM ITEM_SUMBER_DAYA")
            fetched = cursor.fetchall()
            for i in range(len(fetched)):
                response['code'].append([fetched[i][0]])
    return render(request, 't2/permohonan-create.html', response)

def permohonan_update(request, kode):
    pass

def permohonan_delete(request, kode):
    try:
        if request.session['role_this_user'] == 'PETUGAS_FASKES':
            with connection.cursor() as cursor:
                query_delete = "DELETE FROM PERMOHONAN_SUMBER_DAYA_FASKES WHERE no_transaksi_sumber_daya = '{}';"
                cursor.execute(query_delete.format(kode))

            messages.info(request, 'Kode TREQ{} telah dihapus'.format(kode))
            return HttpResponseRedirect('/sumberdaya/permohonan')
    except:
        return HttpResponseRedirect('/sumberdaya/permohonan')

#Trigger 4
def change_status(request, kode):
    if kode == "FIN":
        pass
    if kode == "MAS":
        pass
    if kode == "REJ":
        pass
    if kode == "PRO":
        pass
    pass

#Trigger 5

def item_read(request):
    response = {}
    if request.session['member_id'] != None:
        response['is_authenticated'] = True
    response['items'] = []
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM ITEM_SUMBER_DAYA")
        fetched = cursor.fetchall()
        for i in range(len(fetched)):
            response['items'].append([
                fetched[i][0], fetched[i][1], fetched[i][2],
                fetched[i][3], fetched[i][4], fetched[i][5]
            ])
    return render(request, 't2/item-read.html', response)

def item_create(request):
    pass

def item_update(request):
    pass

def item_delete(request, kodo):
    try:
        if request.session['role_this_user'] == 'SUPPLIER':
            with connection.cursor() as cursor:
                query_delete = "DELETE FROM ITEM_SUMBER_DAYA WHERE kode = '{}';"
                cursor.execute(query_delete.format(kodo))

            messages.info(request, 'Item {} telah dihapus'.format(kode))
            return HttpResponseRedirect('/sumberdaya/item')
    except:
        return HttpResponseRedirect('/sumberdaya/item')