"""sidia_c3 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import include, path

from .views import index_tn6

from .views import view_batch_pengiriman_views, buat_riwayat_pengiriman_views, list_riwayat_pengiriman_views

# Untuk stock warehouse provinsi
from .views import add_stock_warehouse_satgas_views, list_stok_warehouse_satgas_views,update_stok_warehouse_satgas_views , get_lokasi

urlpatterns = [
    #INDEX
        path('', index_tn6, name='index_tn6'),

    #batch prngiriman
        path('view_batch_pengiriman', view_batch_pengiriman_views, name='view_batch_pengiriman_views'),
        path('ubah_status/<kode>', buat_riwayat_pengiriman_views, name='buat_riwayat_pengiriman_views'),
        path('list-riwayat/<kode>', list_riwayat_pengiriman_views, name='list_riwayat_pengiriman_views'),

	# Stock Warehouse Satgas
	    path('tambah-stock-warehouse-satgas/', add_stock_warehouse_satgas_views, name='add_stock_warehouse_satgas_views'),
	    path('get_lokasi/<id_lokasi>', get_lokasi, name='get_lokasi_API'),
		path('list-stock-warehouse-satgas', list_stok_warehouse_satgas_views, name='list_stok_warehouse_satgas_views'),
        path('stock-warehouse-satgas/update/<id_lokasi>', update_stok_warehouse_satgas_views, name='update_stok_warehouse_satgas_views'),

    
]