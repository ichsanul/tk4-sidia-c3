from decimal import Context
from django.conf.urls import url
from django.db.backends.utils import CursorWrapper
from django.http.response import HttpResponse, JsonResponse
from django.shortcuts import redirect, render, HttpResponseRedirect
from django.db import connection # untuk sql
from django.contrib import messages
import json



#Index
def index_tn6(request):
    context = {}
    try:
        if request.session['member_id'] != None:
            context['is_authenticated'] = True
            # Buat otoritas tiap role
            if request.session['role_this_user'] == 'ADMIN_SATGAS':
                context['is_admin_satgas'] = True
            if request.session['role_this_user'] == 'SUPPLIER':
                context['is_supplier'] = True
            if request.session['role_this_user'] == 'PETUGAS_DISTRIBUSI':
                context['is_petugas_distribusi'] = True
            if request.session['role_this_user'] == 'PETUGAS_FASKES':
                context['is_petugas_faskes'] = True

    except:
        pass

    return render(request, 'tampilan.html', context)

#tambah stok warehouse provinsi
def add_stock_warehouse_satgas_views(request):
    context = {}

    if request.method == "POST":
        id_lokasi_warehouse = request.POST['id_lokasi']
        jumlah = request.POST['jumlah']
        kode_item_sumber_daya = request.POST['kode_item_SD']

        with connection.cursor() as cursor:
            query_insert_stok_warehouse_provinsi = "INSERT INTO STOK_WAREHOUSE_PROVINSI VALUES ('{}', '{}', '{}')" 
            cursor.execute(query_insert_stok_warehouse_provinsi.format(id_lokasi_warehouse, jumlah, kode_item_sumber_daya)) 

        return redirect('/t6/list-stock-warehouse-satgas')

    try:
        if request.session['role_this_user'] == 'ADMIN_SATGAS':
            with connection.cursor() as cursor:
                cursor.execute("SELECT id_lokasi from WAREHOUSE_PROVINSI;")
                list_lokasi = cursor.fetchall()
                context['list_id_lokasi'] = list_lokasi

                cursor.execute("""
                SELECT kode, nama 
                FROM ITEM_SUMBER_DAYA;
                """)
                kode_item = cursor.fetchall()
                context['list_kode_item'] = kode_item

                context['is_authenticated'] = True
            return render(request, 'tambah_stok_warehouse_satgas.html', context)
    except:
        pass
    return redirect("/")

#list stok warehouse provinsi
def list_stok_warehouse_satgas_views(request):
    context = {}

    try:
        this_username = request.session['member_id']

        with connection.cursor() as cursor:
            cursor.execute(
            """
            SELECT l.provinsi, l.KabKot, l.Kecamatan, 
                l.Kelurahan, l.Jalan_no, swp.jumlah, i.nama, l.id
            FROM WAREHOUSE_PROVINSI wp join LOKASI l on wp.id_lokasi=l.id join STOK_WAREHOUSE_PROVINSI swp on swp.id_lokasi_warehouse=wp.id_lokasi
                join ITEM_SUMBER_DAYA i on swp.kode_item_sumber_daya=i.kode """
            )
            row_all = cursor.fetchall()
        
        context['list_stok_warehouse_satgas'] = row_all
        context['is_authenticated'] = True
        
        if request.session['role_this_user'] == 'ADMIN_SATGAS':
            context['is_admin'] = True
        elif request.session['role_this_user'] == 'PETUGAS_FASKES':
            context['is_admin'] = False
        else:
            return redirect("/")

        return render(request, 'list_stok_warehouse_provinsi.html', context)
    except:
        return redirect("/")

#update stok warehouse provinsi
def update_stok_warehouse_satgas_views(request, id_lokasi):
    context = {}
    context['lokasi'] = id_lokasi
    if request.method == "POST":
        id_lokasi_warehouse = request.POST['id_lokasi']
        jumlah = request.POST['jumlah']
        kode_item_sumber_daya = request.POST['kode_item_SD']

        with connection.cursor() as cursor:
            query_update_stok_warehouse_provinsi = """
            UPDATE STOK_WAREHOUSE_PROVINSI 
            SET jumlah = '{}',
                kode_item_sumber_daya = '{}'
            WHERE id_lokasi_warehouse = '{}';
            """ 
            cursor.execute(query_update_stok_warehouse_provinsi.format(jumlah, kode_item_sumber_daya, id_lokasi_warehouse)) 

        return redirect('/t6/list-stock-warehouse-satgas')

    try:
        if request.session['role_this_user'] == 'ADMIN_SATGAS':
            
            with connection.cursor() as cursor:
                cursor.execute("SELECT id_lokasi from WAREHOUSE_PROVINSI;")
                list_lokasi = cursor.fetchall()
                context['list_id_lokasi'] = list_lokasi

                cursor.execute("""
                SELECT kode, nama 
                FROM ITEM_SUMBER_DAYA;
                """)
                kode_item = cursor.fetchall()
                context['list_kode_item'] = kode_item

                context['is_authenticated'] = True
                
            return render(request, 'edit_stok_warehouse_provinsi.html', context)
    except:
        pass
    return redirect("/")

#list batch pengiriman
def view_batch_pengiriman_views(request):
    context = {}
    try:
        if request.session['role_this_user'] == 'PETUGAS_FASKES' or request.session['role_this_user'] == 'PETUGAS_DISTRIBUSI':
            with connection.cursor() as cursor:
                query_list_psd = """
                SELECT psdf.No_transaksi_sumber_daya, p.Nama, f.Kode_faskes_nasional, tsd.Total_berat, 
                    rsp.Kode_status_permohonan, rspb.Kode_status_batch_pengiriman
                FROM PERMOHONAN_SUMBER_DAYA_FASKES psdf join PENGGUNA p on psdf.Username_petugas_faskes=p.Username
                    join PETUGAS_FASKES pf on p.Username=pf.Username
                    join FASKES f on pf.Username=f.Username_petugas
                    join TRANSAKSI_SUMBER_DAYA tsd on psdf.No_transaksi_sumber_daya=tsd.Nomor
                    join RIWAYAT_STATUS_PERMOHONAN rsp on psdf.No_transaksi_sumber_daya=rsp.Nomor_permohonan
                    join BATCH_PENGIRIMAN bp on tsd.Nomor=bp.nomor_transaksi_sumber_daya
                    join RIWAYAT_STATUS_PENGIRIMAN rspb on bp.Kode=rspb.Kode_batch
                """
                cursor.execute(query_list_psd)
                row = cursor.fetchall()
                context['list'] = row

                context['is_authenticated'] = True

            if request.session['role_this_user'] == 'PETUGAS_DISTRIBUSI':
                context['is_admin'] = True
            elif request.session['role_this_user'] == 'PETUGAS_FASKES':
                context['is_admin'] = False
            else:
                return redirect("/")

            return render(request, "list_batch.html", context)
    except:
        pass
    return redirect('/')

def buat_riwayat_pengiriman_views(request, kode):
    context = {}
    if request.method == "POST":
        kode_status_batch_pengiriman = request.POST['status']
        kode_batch = request.POST['kode_batch']
        tanggal = request.POST['waktu']

        with connection.cursor() as cursor:
            query_insert_status = "INSERT INTO RIWAYAT_STATUS_PENGIRIMAN VALUES ('{}', '{}', '{}')" 
            cursor.execute(query_insert_status.format(kode_status_batch_pengiriman, kode_batch, tanggal)) 

        return redirect('/satgas/batch_pengiriman')

    try:
        if request.session['role_this_user'] == 'ADMIN_SATGAS':
            
            with connection.cursor() as cursor:

                cursor.execute("SELECT kode, nama from STATUS_BATCH_PENGIRIMAN;")
                list_status = cursor.fetchall()
                context['list_status_pengiriman'] = list_status

                cursor.execute("SELECT CURRENT_DATE;")
                tanggal = cursor.fetchall()[0][0]
                context['waktu'] = tanggal

                query_cari_batch = """
                    SELECT kode FROM batch_pengiriman WHERE nomor_transaksi_sumber_daya = %s
                """
                cursor.execute(query_cari_batch, [kode])
                k_batch = cursor.fetchall()[0][0]
                context['kode_batch'] = k_batch

                context['is_authenticated'] = True
                
            return render(request, 'tambah_riwayat_pengiriman.html', context)
    except:
        pass
    return redirect("/")

def list_riwayat_pengiriman_views(request, kode):
    context = {}
    if request.session['role_this_user'] == 'ADMIN_SATGAS' or request.session['role_this_user'] == 'PETUGAS_DISTRIBUSI':
        with connection.cursor() as cursor:
            query_daftar_riwayat = """
                        SELECT rsp.kode_status_batch_pengiriman, sbp.nama, bp.username_satgas, rsp.tanggal
                        FROM RIWAYAT_STATUS_PENGIRIMAN rsp
                        JOIN STATUS_BATCH_PENGIRIMAN sbp ON sbp.kode = rsp.kode_status_batch_pengiriman
                        JOIN BATCH_PENGIRIMAN bp ON bp.kode = rsp.kode_batch
                        WHERE bp.nomor_transaksi_sumber_daya= %s;
                        """
            cursor.execute(query_daftar_riwayat,[kode])
            daf_riw = cursor.fetchall()
            context['daftar_riwayat'] = daf_riw

        context['is_admin'] = True
        context['is_authenticated'] = True

        return render(request, 'list_riwayat.html', context)

#buat lokasi untuk link
def get_lokasi(request, id_lokasi):
    with connection.cursor() as cursor:
        cursor.execute(
        """
        SELECT id_lokasi 
        FROM ITEM_SUMBER
        WHERE id_lokasi = %s
        """, [id_lokasi]
        )
       
        row_all = cursor.fetchall()

        jsonObj = json.dumps(row_all)
        return HttpResponse(jsonObj, content_type="application/json")
