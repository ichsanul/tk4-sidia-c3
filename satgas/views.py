from decimal import Context
from django.conf.urls import url
from django.db.backends.utils import CursorWrapper
from django.http.response import HttpResponse, JsonResponse
from django.shortcuts import redirect, render, HttpResponseRedirect
from django.db import connection # untuk sql
from django.contrib import messages
import json

"""
Index
"""
def index_tn5(request):
    context = {}
    try:
        if request.session['member_id'] != None:
            context['is_authenticated'] = True
            # Buat otoritas tiap role
            if request.session['role_this_user'] == 'ADMIN_SATGAS':
                context['is_admin_satgas'] = True
            if request.session['role_this_user'] == 'SUPPLIER':
                context['is_supplier'] = True
            if request.session['role_this_user'] == 'PETUGAS_DISTRIBUSI':
                context['is_petugas_distribusi'] = True
            if request.session['role_this_user'] == 'PETUGAS_FASKES':
                context['is_petugas_faskes'] = True

    except:
        pass

    return render(request, 'tn5/index.html', context)

"""
Nomer 12
"""
def add_faskes_views(request):
    context = {}

    if request.method == "POST":
        kode_faskes_nasional = request.POST['kode_faskes']
        id_lokasi = request.POST['id_lokasi']
        username_petugas = request.POST['petugas_faskes']
        kode_tipe_faskes = request.POST['tipe_faskes']

        with connection.cursor() as cursor:
            query_insert_faskes = "INSERT INTO FASKES VALUES ('{}', '{}', '{}', '{}')" 
            cursor.execute(query_insert_faskes.format(kode_faskes_nasional, id_lokasi, username_petugas, kode_tipe_faskes)) 

        return redirect('/satgas/list-faskes')

    try:
        if request.session['role_this_user'] == 'ADMIN_SATGAS':
            with connection.cursor() as cursor:
                cursor.execute("SELECT * from LOKASI;")
                list_lokasi = cursor.fetchall()
                context['list_id_lokasi'] = list_lokasi

                cursor.execute("""
                SELECT p.username, p.nama 
                FROM PETUGAS_FASKES pf join PENGGUNA p on pf.username=p.username
                WHERE p.username IN (
                    (SELECT username from PETUGAS_FASKES) 
                    EXCEPT 
                    (SELECT username_petugas FROM FASKES)
                );
                """)
                petugas_faskes = cursor.fetchall()
                context['list_petugas_faskes'] = petugas_faskes

                cursor.execute("""
                SELECT tf.kode, tf.nama_tipe
                FROM FASKES f join TIPE_FASKES tf on f.kode_tipe_faskes=tf.Kode;
                """)
                list_faskes = cursor.fetchall()
                
                context['list_faskes'] = list_faskes
                context['is_authenticated'] = True
            return render(request, 'satgas/tambah_faskes.html', context)
    except:
        pass
    return redirect("/")

def list_faskes_views(request):
    context = {}

    try:
        this_username = request.session['member_id']

        with connection.cursor() as cursor:
            cursor.execute(
            """
            SELECT f.Kode_faskes_nasional, l.provinsi, l.KabKot, l.Kecamatan, 
                l.Kelurahan, l.Jalan_no, p.nama, tp.nama_tipe 
            FROM FASKES f join LOKASI l on f.id_lokasi=l.id join PETUGAS_FASKES pf on f.username_petugas=pf.username 
                join PENGGUNA p on pf.username=p.username join TIPE_FASKES tp on f.kode_tipe_faskes=tp.kode;"""
            )
            row_all = cursor.fetchall()
        
        context['list_faskes'] = row_all
        context['is_authenticated'] = True
        
        if request.session['role_this_user'] == 'ADMIN_SATGAS':
            context['is_admin'] = True
        elif request.session['role_this_user'] == 'SUPPLIER':
            context['is_admin'] = False
        else:
            return redirect("/")

        return render(request, 'satgas/list_faskes.html', context)
    except:
        return redirect("/")


def update_faskes_views(request, kode_faskes):
    context = {}
    context['kode'] = kode_faskes

    if request.method == "POST":
        kode_faskes_nasional = request.POST['kode_faskes']
        id_lokasi = request.POST['id_lokasi']
        username_petugas = request.POST['petugas_faskes']
        kode_tipe_faskes = request.POST['tipe_faskes']

        with connection.cursor() as cursor:
            query_update_faskes = """
            UPDATE FASKES 
            SET id_lokasi = '{}',
                username_petugas = '{}',
                kode_tipe_faskes = '{}'
            WHERE kode_faskes_nasional = '{}';
            """ 
            cursor.execute(query_update_faskes.format(id_lokasi, username_petugas, kode_tipe_faskes, kode_faskes_nasional)) 

        return redirect('/satgas/list-faskes')

    try:
        if request.session['role_this_user'] == 'ADMIN_SATGAS':
            
            with connection.cursor() as cursor:
                cursor.execute("SELECT * from LOKASI;")
                list_lokasi = cursor.fetchall()
                context['list_id_lokasi'] = list_lokasi

                cursor.execute("""
                SELECT p.username, p.nama 
                FROM PETUGAS_FASKES pf join PENGGUNA p on pf.username=p.username
                WHERE p.username IN (
                    (SELECT username from PETUGAS_FASKES) 
                    EXCEPT 
                    (SELECT username_petugas FROM FASKES)
                );
                """)
                petugas_faskes = cursor.fetchall()
                context['list_petugas_faskes'] = petugas_faskes

                cursor.execute("""
                SELECT tf.kode, tf.nama_tipe
                FROM FASKES f join TIPE_FASKES tf on f.kode_tipe_faskes=tf.Kode;
                """)
                list_faskes = cursor.fetchall()
                
                context['list_faskes'] = list_faskes
                context['is_authenticated'] = True
                
            return render(request, 'satgas/edit_faskes.html', context)
    except:
        pass
    return redirect("/")

def delete_faskes_views(request, kode_faskes):
    try:
        if request.session['role_this_user'] == 'ADMIN_SATGAS':
            with connection.cursor() as cursor:
                query_delete = "DELETE FROM FASKES WHERE kode_faskes_nasional = '{}';"
                cursor.execute(query_delete.format(kode_faskes))

            messages.info(request, 'Kode {} telah dihapus'.format(kode_faskes))
            return HttpResponseRedirect('/satgas/list-faskes')
    except:
        pass

    return redirect('/')

def get_lokasi(request, id_lokasi):
    with connection.cursor() as cursor:
        cursor.execute(
        """
        SELECT * 
        FROM LOKASI
        WHERE id = %s
        """, [id_lokasi]
        )
       
        row_all = cursor.fetchall()

        jsonObj = json.dumps(row_all)
        return HttpResponse(jsonObj, content_type="application/json")

"""
Nomer 13
"""
def add_warehouse_satgas_views(request):
    context = {}

    if request.method == "POST":
        provinsi = request.POST['provinsi']
        id_lokasi = request.POST['id_lokasi']

        with connection.cursor() as cursor:
            query_insert_faskes = "INSERT INTO WAREHOUSE_PROVINSI VALUES ('{}')" 
            cursor.execute(query_insert_faskes.format(id_lokasi)) 

        return redirect('/satgas/list-warehouse')

    try:
        if request.session['role_this_user'] == 'ADMIN_SATGAS':
            with connection.cursor() as cursor:
                cursor.execute("""
                (SELECT DISTINCT provinsi 
                FROM LOKASI)
                EXCEPT 
                (SELECT l.provinsi
                FROM LOKASI l join WAREHOUSE_PROVINSI wp on l.id=wp.Id_lokasi
                );
                """)
                list_provinsi = cursor.fetchall()
                context['list_provinsi'] = list_provinsi

                cursor.execute("SELECT id from LOKASI WHERE id not in (select * from WAREHOUSE_PROVINSI);")
                list_lokasi = cursor.fetchall()
                context['list_id_lokasi'] = list_lokasi
                
                context['is_authenticated'] = True
            return render(request, 'warehouse_satgas/tambah_warehouse.html', context)
    except:
        pass
    return redirect("/")

def get_id_lokasi_from(provinsi):
    # return list of tuple
    with connection.cursor() as cursor:
        cursor.execute("SELECT id from LOKASI WHERE provinsi= %s", [provinsi])
        row_all = cursor.fetchall()
        return row_all

def get_id_lokasi_from_provinsi(request, provinsi):
    row_all = get_id_lokasi_from(provinsi)
    
    jsonObj = json.dumps(row_all)
    return HttpResponse(jsonObj, content_type="application/json")

def list_warehouse_views(request):
    context = {}
    try:
        this_username = request.session['member_id']

        with connection.cursor() as cursor:
            cursor.execute(
            """
            SELECT l.provinsi, l.KabKot, l.Kecamatan, 
                l.Kelurahan, l.Jalan_no  
            FROM WAREHOUSE_PROVINSI ws join LOKASI l on ws.id_lokasi=l.id 
            ;"""
            )
            row_all = cursor.fetchall()
        
        context['list_faskes'] = row_all
        context['is_authenticated'] = True
        
        if request.session['role_this_user'] == 'ADMIN_SATGAS':
            context['is_admin'] = True
        elif request.session['role_this_user'] == 'SUPPLIER':
            context['is_admin'] = False
        else:
            return redirect("/")

        return render(request, 'warehouse_satgas/list_warehouse.html', context)
    except:
        return redirect("/")


def update_warehouse_views(request, provinsi):
    context = {}
    context['kode'] = provinsi

    if request.method == "POST":
        provinsi = request.POST['provinsi']
        id_lokasi = request.POST['id_lokasi']
        
        with connection.cursor() as cursor:
            query_delete = """
            DELETE 
            FROM WAREHOUSE_PROVINSI 
            WHERE id_lokasi = 
                (SELECT id FROM LOKASI l join WAREHOUSE_PROVINSI wp on l.id=wp.id_lokasi AND l.provinsi = %s);
            """
            cursor.execute(query_delete, [provinsi])
            
            query_update = """
            INSERT INTO WAREHOUSE_PROVINSI VALUES
            (%s);
            """ 
            cursor.execute(query_update, [id_lokasi])

        return redirect('/satgas/list-warehouse')

    try:
        if request.session['role_this_user'] == 'ADMIN_SATGAS':
            
            list_lokasi = get_id_lokasi_from(provinsi)
            context['list_id_lokasi'] = list_lokasi
                
            context['is_authenticated'] = True
                
            return render(request, 'warehouse_satgas/edit_warehouse.html', context)
    except:
        pass
    return redirect("/")

def delete_warehouse_views(request, kode):
    try:
        if request.session['role_this_user'] == 'ADMIN_SATGAS':
            with connection.cursor() as cursor:
                query_delete = """
                    DELETE 
                    FROM WAREHOUSE_PROVINSI 
                    WHERE id_lokasi in
                        (SELECT id FROM LOKASI l join WAREHOUSE_PROVINSI wp on l.id=wp.id_lokasi AND l.provinsi = %s);
                    """
                cursor.execute(query_delete, [kode])

            messages.info(request, 'Data berhasil dihapus')
            return redirect('/satgas/list-warehouse')
    except:
        pass

    return redirect('/')

"""
Nomer 14
"""

def batch_pengiriman_views(request):
    context = {}
    try:
        if request.session['role_this_user'] == 'ADMIN_SATGAS':
            with connection.cursor() as cursor:
                query_list_psd = """
                SELECT psdf.No_transaksi_sumber_daya, p.Nama, f.Kode_faskes_nasional, tsd.Total_berat, 
                    rsp.Kode_status_permohonan, rspb.Kode_status_batch_pengiriman
                FROM PERMOHONAN_SUMBER_DAYA_FASKES psdf join PENGGUNA p on psdf.Username_petugas_faskes=p.Username
                    join PETUGAS_FASKES pf on p.Username=pf.Username
                    join FASKES f on pf.Username=f.Username_petugas
                    join TRANSAKSI_SUMBER_DAYA tsd on psdf.No_transaksi_sumber_daya=tsd.Nomor
                    join RIWAYAT_STATUS_PERMOHONAN rsp on psdf.No_transaksi_sumber_daya=rsp.Nomor_permohonan
                    join BATCH_PENGIRIMAN bp on tsd.Nomor=bp.nomor_transaksi_sumber_daya
                    join RIWAYAT_STATUS_PENGIRIMAN rspb on bp.Kode=rspb.Kode_batch
                """
                cursor.execute(query_list_psd)
                row = cursor.fetchall()
                context['list'] = row

                context['is_admin'] = True
                context['is_authenticated'] = True

            return render(request, "batch_pengiriman/list_batch_pengiriman.html", context)
    except:
        pass
    return redirect('/')

def create_batch_pengiriman(request, no_tsd):
    context = {}
    with connection.cursor() as cursor:
        cek_status = """
            SELECT kode_status_permohonan
            FROM RIWAYAT_STATUS_PERMOHONAN 
            WHERE Nomor_permohonan = %s
        """
        cursor.execute(cek_status, no_tsd)
        cek_status = cursor.fetchall()[0][0]
        
        if cek_status == "PRO":
            context['is_processing'] = True
        elif cek_status == 'FIN' or cek_status == 'MAS':
            context['is_processing'] = False
        else:
            return redirect('/')

    if request.method == "POST":
        kode = request.POST['kode']
        petugas_satgas = request.session['member_id']
        petugas_distribusi = request.POST['petugas_distribusi']
        nomor = request.POST['nomor']
        kendaraan = request.POST['kendaraan']

        tanda_terima = str(kode) +"-" + str(nomor)

        with connection.cursor() as cursor:
            query_insert = """
            INSERT INTO BATCH_PENGIRIMAN(Kode, Username_satgas, Username_petugas_distribusi,
                Tanda_terima, nomor_transaksi_sumber_daya, No_kendaraan) VALUES
            (%s, %s, %s, %s, %s, %s)
            """
            cursor.execute(query_insert, [kode, petugas_satgas, petugas_distribusi, \
                tanda_terima, nomor, kendaraan])    

        return redirect('/satgas/create_batch_pengiriman/no-transaksi-{}'.format(no_tsd))

    try:
        if request.session['role_this_user'] == 'ADMIN_SATGAS':
            with connection.cursor() as cursor:
                petugas_satgas = request.session['member_id']
                context['petugas_satgas'] = petugas_satgas
                context['nomor'] = no_tsd

                query_berat = """
                SELECT tsd.Total_berat 
                FROM PERMOHONAN_SUMBER_DAYA_FASKES psdf 
                    join TRANSAKSI_SUMBER_DAYA tsd on psdf.No_transaksi_sumber_daya=tsd.Nomor
                WHERE No_transaksi_sumber_daya = %s;
                """
                cursor.execute(query_berat, [no_tsd])
                total_berat = cursor.fetchall()
                context['berat'] = total_berat

                query_kode = "select kode from batch_pengiriman"
                cursor.execute(query_kode)
                kode = cursor.fetchall()
                kode = get_kode_baru(kode)
                kode_modif = kode[:3] + str(int(kode[3:])+1)
                context['kode'] = kode_modif

                query_kendaraan = """
                SELECT Nomor, Nama
                FROM KENDARAAN
                WHERE NOT EXISTS 
                    (SELECT rsp.Kode_status_batch_pengiriman 
                        FROM RIWAYAT_STATUS_PENGIRIMAN rsp 
                            join BATCH_PENGIRIMAN bp on rsp.Kode_batch=bp.Kode
                        WHERE bp.No_kendaraan=Nomor and 
                            (rsp.Kode_status_batch_pengiriman='PRO' or rsp.Kode_status_batch_pengiriman='OTW')
                    )
                ;
                """
                cursor.execute(query_kendaraan)
                list_kendaraan = cursor.fetchall()
                context['list_kendaraan'] = list_kendaraan

                query_petugas_distribusi = """
                SELECT pd.username, p.Nama
                FROM PETUGAS_DISTRIBUSI pd 
                    join PENGGUNA p on pd.username=p.username
                WHERE NOT EXISTS 
                    (SELECT rsp.Kode_status_batch_pengiriman 
                        FROM RIWAYAT_STATUS_PENGIRIMAN rsp 
                            join BATCH_PENGIRIMAN bp on rsp.Kode_batch=bp.Kode
                        WHERE bp.Username_petugas_distribusi=pd.username and 
                            (rsp.Kode_status_batch_pengiriman='PRO' or rsp.Kode_status_batch_pengiriman='OTW')
                    )
                ;
                """
                cursor.execute(query_petugas_distribusi)
                list_petugas_distribusi = cursor.fetchall()
                context['list_petugas_distribusi'] = list_petugas_distribusi

                # Bagian tabel daftar batch
                query_daftar_batch = """
                SELECT kode, no_kendaraan, username_petugas_distribusi, 
                    id_lokasi_asal, id_lokasi_tujuan
                FROM BATCH_PENGIRIMAN
                WHERE nomor_transaksi_sumber_daya=%s
                """
                cursor.execute(query_daftar_batch, [no_tsd])
                daftar_batch = cursor.fetchall()
                context['daftar_batch'] = daftar_batch

            context['is_admin'] = True
            context['is_authenticated'] = True
            
            return render(request, 'batch_pengiriman/buat_batch_pengiriman.html', context)
    except:
        pass
    return render('/')

def get_kode_baru(list_kode):
    temp = "pbd0"
    if len(list_kode) == 0:
        return "pbd0"
    for i in list_kode:
        if len(i[0]) > len(temp):
            temp = i[0]
        elif len(i[0]) == len(temp):
            temp = max(i[0], temp)
    return temp

def simpan_daftar_batch_pengiriman(request, no_tsd):
    context = {}
    if request.method == 'POST':
        # Ada trigger nomer 6
        return redirect('/satgas/batch_pengiriman')
    
    return redirect('/')