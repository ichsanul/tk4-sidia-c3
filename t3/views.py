from decimal import Context
from django.conf.urls import url
from django.db.backends.utils import CursorWrapper
from django.http.response import HttpResponse, JsonResponse
from django.shortcuts import redirect, render, HttpResponseRedirect
from django.db import connection # untuk sql
from django.contrib import messages
import json

# Create your views here.
"""
Index
"""
def index(request):
    context = {}
    try:
        if request.session['member_id'] != None:
            context['is_authenticated'] = True
            # Buat otoritas tiap role
            if request.session['role_this_user'] == 'ADMIN_SATGAS':
                context['is_admin_satgas'] = True
            if request.session['role_this_user'] == 'SUPPLIER':
                context['is_supplier'] = True
            if request.session['role_this_user'] == 'PETUGAS_DISTRIBUSI':
                context['is_petugas_distribusi'] = True
            if request.session['role_this_user'] == 'PETUGAS_FASKES':
                context['is_petugas_faskes'] = True
    except:
        pass

    return render(request, "t3/index.html", context)
    

"""
Nomer 6
"""
def tambah_psd(request):
    context = {}

    if request.method == "POST":
        no_transaksi = request.POST['no_transaksi']
        Petugas = request.POST['Petugas']
        Supplier = request.POST['Supplier']

        no_urut = request.POST['Supplier']
        kode = request.POST['kode']
        jumlah_item = request.POST['kode']
        
        with connection.cursor() as cursor:
            query_insert_faskes = "INSERT INTO FASKES VALUES ('{}', '{}', '{}', '{}')" 
            cursor.execute(query_insert_faskes.format(kode_faskes_nasional, id_lokasi, username_petugas, kode_tipe_faskes)) 

        return redirect('/satgas/list-faskes')

    
    if request.session['role_this_user'] == 'ADMIN_SATGAS':
        with connection.cursor() as cursor:
            cursor.execute("SELECT nomor_pesanan from PESANAN_SUMBER_DAYA;")
            list_no_transaksi = cursor.fetchall()
            context['no_transaksi'] = max(list_no_transaksi)[0] + 1

            petugas_satgas = request.session['member_id']
            context['Petugas'] = petugas_satgas

            cursor.execute("SELECT Username_supplier from ITEM_SUMBER_DAYA")
            Supplier = cursor.fetchall()
            context['Supplier'] = Supplier

            # Tambah 
            cursor.execute("SELECT Nomor from TRANSAKSI_SUMBER_DAYA;")
            no_urut = cursor.fetchall()
            context['no_urut'] = max(no_urut)[0] + 1

            cursor.execute("select Kode_tipe_item from ITEM_SUMBER_DAYA")
            list_kode = cursor.fetchall()
            context['list_kode'] = list_kode
    
            context['is_authenticated'] = True
        return render(request, 'psd/tambah_psd.html', context)
    
    return redirect("/")
