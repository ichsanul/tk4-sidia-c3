from django.apps import AppConfig


class PetugasFaskesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 't3'
