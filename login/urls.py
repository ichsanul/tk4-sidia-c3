"""sidia_c3 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import include, path

from .views import about, login_views, register_satgas_views, index_views, logout_views, register_supplier_views, register_petugas_faskes_views, register_petugas_distribusi_views
from .views import edit

urlpatterns = [
    path('', index_views, name='index_views'),
    path('about', about, name='about'),
    path('login', login_views, name='login_views'),
    path('logout', logout_views, name='logout_views'),
    path('register-admin', register_satgas_views, name='register_satgas_views'),
    path('register-supplier', register_supplier_views, name='register_supplier_views'),
    path('register-petugas-distribusi', register_petugas_distribusi_views, name='register_petugas_distribusi_views'),
    path('register-faskes', register_petugas_faskes_views, name='register_petugas_faskes_views'),

    path('edit', edit, name='edit'),
    # To-do
]
