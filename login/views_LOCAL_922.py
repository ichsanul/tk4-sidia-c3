from django.shortcuts import render, redirect, HttpResponse
from django.db import connection # untuk sql

def check_account(email, password):
    # Untuk login
    with connection.cursor() as cursor:
        cursor.execute("SELECT username, password from pengguna where username = %s;", [email])
        try:
            row_user_general = cursor.fetchall()[0]
        except IndexError:
            return None


        if row_user_general[0] == email and row_user_general[1] == password:
            # Kalo password bener, cari groupnya

            dict_username = {}
            
            cursor.execute("SELECT username from ADMIN_SATGAS where username = %s;", [email])
            row_satgas = cursor.fetchall()
            dict_username['ADMIN_SATGAS'] = row_satgas

            cursor.execute("SELECT username from SUPPLIER where username = %s;", [email])
            row_supplier = cursor.fetchall()
            dict_username['SUPPLIER'] = row_supplier
            
            cursor.execute("SELECT username from PETUGAS_DISTRIBUSI where username = %s;", [email])
            row_petugas_distribusi = cursor.fetchall()
            dict_username['PETUGAS_DISTRIBUSI'] = row_petugas_distribusi

            cursor.execute("SELECT username from PETUGAS_FASKES where username = %s;", [email])
            row_petugas_faskes = cursor.fetchall()
            dict_username['PETUGAS_FASKES'] = row_petugas_faskes

            print(dict_username)

            if dict_username['ADMIN_SATGAS'] != []:
                return 'ADMIN_SATGAS'
            elif dict_username['SUPPLIER'] != []:
                return 'SUPPLIER'
            elif dict_username['PETUGAS_DISTRIBUSI'] != []:
                return 'PETUGAS_DISTRIBUSI'
            elif dict_username['PETUGAS_FASKES'] != []:
                return 'PETUGAS_FASKES'

        # Kalo salah password
        return None

def login_views(request):
    try:
        if request.session['member_id'] != None:
            return redirect('/')
    except:
        pass

    if request.method == "POST":
        email = request.POST['email']
        password = request.POST['psw']

        this_user_role = check_account(email, password)
        if this_user_role == None:
            return render(request, 'login.html', {'false_password':True})
        elif this_user_role == 'ADMIN_SATGAS':
            request.session['role_this_user'] = 'ADMIN_SATGAS'
        elif this_user_role == 'SUPPLIER':
            request.session['role_this_user'] = 'SUPPLIER'
        elif this_user_role == 'PETUGAS_DISTRIBUSI':
            request.session['role_this_user'] = 'PETUGAS_DISTRIBUSI'
        elif this_user_role == 'PETUGAS_FASKES':
            request.session['role_this_user'] = 'PETUGAS_FASKES'
        
        request.session['member_id'] = email
        return redirect('/')

    return render(request, 'login.html')

def logout_views(request):
    try:
        del request.session['member_id']
        del request.session['role_this_user']
        
        return redirect('index_views')
    except KeyError:
        pass
    return HttpResponse("You're logged out.")

def register_satgas_views(request):
    context = {}

    if request.method == "POST":
        Username = request.POST['email']
        Password = request.POST['psw']
        Nama = request.POST['nama']
        Alamat_Kel = request.POST['Kelurahan']
        Alamat_Kec = request.POST['Kecamatan']
        Alamat_KabKot = request.POST['kbkt']
        provinsi = request.POST['provinsi']
        No_telepon = request.POST['no_telp']

        with connection.cursor() as cursor:
            cursor.execute("SELECT username from pengguna where username = %s;", [Username])
            row = cursor.fetchall()

            try:
                if Username in row[0]:
                    context['is_registered'] = True 
                    context['list_provinsi'] = LIST_PROVINSI
                    return render(request, 'register_admin_satgas.html', context)
            except:
                query_insert_pengguna = "INSERT INTO PENGGUNA VALUES ('{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}')" 
                query_insert_admin_satgas = "INSERT INTO ADMIN_SATGAS VALUES ('{}')" 
                
                cursor.execute(query_insert_pengguna.format(Username, Password, Nama, Alamat_Kel, Alamat_Kec, Alamat_KabKot, provinsi, No_telepon))
                cursor.execute(query_insert_admin_satgas.format(Username))

                return redirect('/login')

    context['list_provinsi'] = LIST_PROVINSI
    return render(request, 'register_admin_satgas.html', context)

def register_supplier_views(request):
    context = {}

    if request.method == "POST":
        Nama_organisasi = request.POST['nama_org']
        Username = request.POST['email']
        Password = request.POST['psw']
        Nama = request.POST['nama']
        Alamat_Kel = request.POST['Kelurahan']
        Alamat_Kec = request.POST['Kecamatan']
        Alamat_KabKot = request.POST['kbkt']
        provinsi = request.POST['provinsi']
        No_telepon = request.POST['no_telp']

        with connection.cursor() as cursor:
            cursor.execute("SELECT username from pengguna where username = %s;", [Username])
            row = cursor.fetchall()

            try:
                if Username in row[0]:
                    context['is_registered'] = True 
                    context['list_provinsi'] = LIST_PROVINSI
                    return render(request, 'register_supplier.html', context)
            except:
                query_insert_pengguna = "INSERT INTO PENGGUNA VALUES ('{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}')" 
                query_insert_supplier = "INSERT INTO SUPPLIER VALUES ('{}', '{}')" 
                
                cursor.execute(query_insert_pengguna.format(Username, Password, Nama, Alamat_Kel, Alamat_Kec, Alamat_KabKot, provinsi, No_telepon))
                cursor.execute(query_insert_supplier.format(Username,Nama_organisasi))

                return redirect('/login')

    context['list_provinsi'] = LIST_PROVINSI
    return render(request, 'register_supplier.html', context)

def register_petugas_distribusi_views(request):
    context = {}

    if request.method == "POST":
        Username = request.POST['email']
        Password = request.POST['psw']
        Nama = request.POST['nama']
        Alamat_Kel = request.POST['Kelurahan']
        Alamat_Kec = request.POST['Kecamatan']
        Alamat_KabKot = request.POST['kbkt']
        provinsi = request.POST['provinsi']
        No_telepon = request.POST['no_telp']
        No_SIM = request.POST['no_sim']

        with connection.cursor() as cursor:
            cursor.execute("SELECT username from pengguna where username = %s;", [Username])
            row = cursor.fetchall()

            try:
                if Username in row[0]:
                    context['is_registered'] = True 
                    context['list_provinsi'] = LIST_PROVINSI
                    return render(request, 'register_petugas_distribusi.html', context)
            except:
                query_insert_pengguna = "INSERT INTO PENGGUNA VALUES ('{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}')" 
                query_insert_petugas_distribusi = "INSERT INTO PETUGAS_DISTRIBUSI VALUES ('{}', '{}')" 
                
                cursor.execute(query_insert_pengguna.format(Username, Password, Nama, Alamat_Kel, Alamat_Kec, Alamat_KabKot, provinsi, No_telepon))
                cursor.execute(query_insert_petugas_distribusi.format(Username, No_SIM))

                return redirect('/login')

    context['list_provinsi'] = LIST_PROVINSI
    return render(request, 'register_petugas_distribusi.html', context)

def register_petugas_faskes_views(request):
    context = {}

    if request.method == "POST":
        Username = request.POST['email']
        Password = request.POST['psw']
        Nama = request.POST['nama']
        Alamat_Kel = request.POST['Kelurahan']
        Alamat_Kec = request.POST['Kecamatan']
        Alamat_KabKot = request.POST['kbkt']
        provinsi = request.POST['provinsi']
        No_telepon = request.POST['no_telp']

        with connection.cursor() as cursor:
            cursor.execute("SELECT username from pengguna where username = %s;", [Username])
            row = cursor.fetchall()

            try:
                if Username in row[0]:
                    context['is_registered'] = True 
                    context['list_provinsi'] = LIST_PROVINSI
                    return render(request, 'register_petugas_faskes.html', context)
            except:
                query_insert_pengguna = "INSERT INTO PENGGUNA VALUES ('{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}')" 
                query_insert_petugas_faskes = "INSERT INTO PETUGAS_FASKES VALUES ('{}', '{}')" 
                
                cursor.execute(query_insert_pengguna.format(Username, Password, Nama, Alamat_Kel, Alamat_Kec, Alamat_KabKot, provinsi, No_telepon))
                cursor.execute(query_insert_petugas_faskes.format(Username))

                return redirect('/login')

    context['list_provinsi'] = LIST_PROVINSI
    return render(request, 'register_petugas_faskes.html', context)

def index_views(request):
    context = {}
    try:
        if request.session['member_id'] != None:
            context['is_authenticated'] = True
            # Buat otoritas tiap role
            if request.session['role_this_user'] == 'ADMIN_SATGAS':
                context['is_admin_satgas'] = True
            if request.session['role_this_user'] == 'SUPPLIER':
                context['is_supplier'] = True
            if request.session['role_this_user'] == 'PETUGAS_DISTRIBUSI':
                context['is_petugas_distribusi'] = True
            if request.session['role_this_user'] == 'PETUGAS_FASKES':
                context['is_petugas_faskes'] = True
            # TODO
            # Tambahin ke index.html
    except:
        pass

    return render(request, 'index.html', context)


LIST_PROVINSI = [
    ('1', 'Aceh'),
    ('2', 'Sumatera Utara'),
    ('3', 'Sumatera Barat'),
    ('4', 'Riau'),
    ('5', 'Kepulauan Riau'),
    ('6', 'Bengkulu'),
    ('7', 'Jambi'),
    ('8', 'Sumatera Selatan'),
    ('9', 'Bangka Belitung'),
    ('10', 'Lampung'),
    ('11', 'Banten'),
    ('12', 'Jawa Barat'),
    ('13', 'Dki Jakarta'),
    ('14', 'Jawa Tengah'),
    ('15', 'Di Yogyakarta'),
    ('16', 'Jawa Timur'),
    ('17', 'Bali'),
    ('18', 'Nusa Tenggara Barat'),
    ('19', 'Nusa Tenggara Timur'),
    ('20', 'Kalimantan Timur'),
    ('21', 'Kalimantan Barat'),
    ('22', 'Kalimantan Selatan'),
    ('23', 'Kalimantan Tengah'),
    ('24', 'Kalimantan Utara'),
    ('25', 'Gorontalo'),
    ('26', 'Sulawesi Barat'),
    ('27', 'Sulawesi Tengah'),
    ('28', 'Sulawesi Selatan'),
    ('29', 'Sulawesi Tenggara'),
    ('30', 'Sulawesi Utara'),
    ('31', 'Maluku'),
    ('32', 'Maluku Utara'),
    ('33', 'Papua Barat'),
    ('34', 'Papua'),
]